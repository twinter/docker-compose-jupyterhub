# Jupyterhub with docker-compose

!!! This is currently severely outdated !!!

This is a ready to use setup of jupyterhub with all files persisted to disk at a configurable location as well as support for named servers and a shared folder between all users and all servers. User authentication is done via OAuth2 with a Nextcloud instance but can be changed easily with a couple of changes in the jupyterhub.env file as well as the jupyterhub_config.py.

It is based on the tutorial from <https://opendreamkit.org/2018/10/17/jupyterhub-docker/> but has a a lot of changes and additional configuration made by me as well as adaptations to my reverse proxy setup (have a look at the repo at <https://gitlab.com/twinter/docker-compose-reverse-proxy> for details).

## important note

Jupyterhub and Jupyterlab should be of the same version. They currently aren't as the currently (07.11.20) available images on dockerhub for jupyterhub is broken.

Relevant tickets for this issue:
- <https://github.com/jupyterhub/jupyterhub/issues/2852>
- <https://github.com/jupyterhub/jupyterhub/issues/2911>

Because of this I am currently using jupyterlab for hub version 1.1 with hub version 1.2 as this combination seems to work mostly as expected. The only thing I've found so far is that the working directory is not set correctly and I've introduced a quick fix for this problem (see jupyterhub_config.py around line 44, there is a note directly above the line).

## Configuration

### Required

Create your own .env file in the root folder and jupyterhub.env file in the jupyterhub folder from the template files in the same folders by filling in the values for your setup and setup your Nextcloud instance as an OAuth2 provider. That's it!

If you're using OAuth2 (using Nextcloud as default provider here), see <https://oauthenticator.readthedocs.io/en/latest/getting-started.html#nextcloud-setup> (loads of other providers listed there as well).
  See <https://docs.nextcloud.com/server/19/admin_manual/configuration_server/oauth2.html> for details on using Nextcloud as OAuth provider.

### Optional

The jupyterhub config file lives in jupyterhub/jupyterhub_config.py, just change it there. You may need to restart jupyterhub for it to take effect.

Run `docker-compose run --rm jupyterhub jupyterhub --generate-config -y=true -f /dev/stdout` to get a list of all config values and the default configuration printed to the terminal.

Remember to restart jupyterhub after changing it's config.

#### Options

The following options are configurable through (optional) environment variables on the jupyterhub container.

- `JUPYTER_USER_UID`: user id of the user in the jupyterlab container
- `JUPYTER_USER_GID`: group id of the user in the jupyterlab container
- `HOST_DATA_PATH`: path on the host where all data will be save, configured in the .env file
- `DOCKER_NOTEBOOK_DIR`: the root working directory for jupyterlab

## ToDo

- remove note and temporary bugfix (see above)
- use the same version for the hub and the lab
- introduce a parameter to docker-compose file to keep version declaration DRY
- add something to automatically delete/cull the data from deleted named servers and servers belonging to deleted users, related ticket: <https://github.com/jupyterhub/jupyterhub-idle-culler/issues/8>
- refine automated culling of inactive servers to avoid killing a server with a long running process, related ticket: <https://github.com/jupyterhub/jupyterhub-idle-culler/issues/10>, use some functionality of the startup script of the jupyterlab instance?

### ToDo rstudio

- inject own notebook config
    - ideally in /etc/jupyter
    - idea: rename original config, copy own config, add simple script with name of original config that calls the original config and the our own
