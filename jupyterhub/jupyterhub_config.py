import os
import sys
import shutil
import json

from oauthenticator.generic import GenericOAuthenticator

c = get_config()


# read values from the environment variables
notebook_dir = os.getenv('DOCKER_NOTEBOOK_DIR', '/home/jovyan/work')  # default place for notebooks in most images
host_data_path = os.environ['HOST_DATA_PATH']
uid = os.getenv('JUPYTER_USER_UID', 1000)
gid = os.getenv('JUPYTER_USER_GID', 100)


# general config
c.JupyterHub.cookie_secret_file = '/srv/jupyterhub/config/jupyterhub_cookie_secret'
c.JupyterHub.db_url = 'sqlite:///config/jupyterhub.sqlite'
c.JupyterHub.default_server_name = os.environ['SERVER_NAME']
c.DockerSpawner.extra_create_kwargs.update({'command': 'start-singleuser.sh'})
c.JupyterHub.allow_named_servers = True  # allow users to create extra named servers


# OAuth config
c.JupyterHub.authenticator_class = GenericOAuthenticator
c.GenericOAuthenticator.client_id = os.environ['NEXTCLOUD-CLIENT-ID']
c.GenericOAuthenticator.client_secret = os.environ['NEXTCLOUD-CLIENT-SECRET-KEY']
c.GenericOAuthenticator.login_service = os.environ['OAUTH2_SERVICE_NAME']
c.GenericOAuthenticator.username_key = lambda r: r.get('ocs', {}).get('data', {}).get('id')


# add admin users
c.JupyterHub.admin_access = True
c.Authenticator.admin_users = admin = set(os.environ['ADMIN_USERS'].split(';'))


# configure spawner
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'
c.DockerSpawner.image = os.environ['DOCKER_JUPYTER_IMAGE']
c.DockerSpawner.network_name = os.environ['DOCKER_NETWORK_NAME']
c.DockerSpawner.notebook_dir = notebook_dir
# TODO: remove fix on the following as soon as it is working as expected again
c.DockerSpawner.environment = {'NOTEBOOK_DIR': notebook_dir}
c.DockerSpawner.remove_containers = True
c.JupyterHub.hub_ip = os.environ['HUB_IP']
# c.DockerSpawner.debug = True


# configure mounting of the notebook folder on the host in the container
def create_dir_hook(spawner):
    """ pre spawn hook method and function based on
    https://github.com/jupyterhub/dockerspawner/issues/160#issuecomment-708651374 """
    
    # generate paths to the data folder for the new container, empty name == not a named server
    if spawner.name == '':
        data_path = os.path.join('notebooks', spawner.user.name, 'personal_server')
    else:
        data_path = os.path.join('notebooks', spawner.user.name, 'named_servers', spawner.name)

    # create and setup user folder if it doesn't exist, this path is inside the jupyterhub
    #  container so we're not using host_data_path here.
    data_path_in_jupyterhub = os.path.join('/srv/jupyterhub', data_path)

    # ensure path on the host exists and set folder permissions so the user inside the jupyterlab container can write
    #  there, this is idempotent so we're just running it always
    os.makedirs(data_path_in_jupyterhub, exist_ok=True)
    os.chown(data_path_in_jupyterhub, uid=uid, gid=gid)
    
    # create a folder shared between all users and make it writable for all of them, this is idempotent
    shared_folder = '/srv/jupyterhub/shared_folder'
    os.makedirs(shared_folder, exist_ok=True)  # not required (path is created by docker) but still here just to be safe
    os.chown(shared_folder, uid=uid, gid=gid)

    # create mounts from the host to the new jupyterlab container
    spawner.volumes = {
        os.path.join(host_data_path, data_path): notebook_dir,
        os.path.join(host_data_path, 'shared_folder'): os.path.join(notebook_dir, 'shared_folder'),
    }

    # this is the default behaviour according to the spec but somehow not active so we set it explicitly here
    if spawner.name == '':
        spawner.name_template = '{prefix}-{username}'
    else:
        spawner.name_template = '{prefix}-{username}__{servername}'


c.DockerSpawner.pre_spawn_hook = create_dir_hook


def post_stop_hook(spawner, *args, **kwargs):
    with open('/srv/jupyterhub/notebooks/test.json', 'w') as f:
        json.dump({
            'remove': spawner.remove,
            'remove_containers': spawner.remove_containers,
            'remove_object': dir(spawner.remove_object),
            'spawner': dir(spawner),
            'state': spawner.get_state(),
            'will_resume': spawner.will_resume,
        }, f)


c.DockerSpawner.post_stop_hook = post_stop_hook


# cull idle containers, for details see https://github.com/jupyterhub/jupyterhub-idle-culler
c.JupyterHub.services = [
    {
        'name':    'idle-culler',
        'admin':   True,
        'command': [
            sys.executable,
            '-m', 'jupyterhub_idle_culler',
            '--timeout=3600'
        ],
    }
]
